import POSConnect from './pos';

const pos = new POSConnect('/dev/ttyUSB0');

pos.setDebug(true);
pos.connect().then(() => {
    console.log('CONNECTED!!!');

    /*
    pos.GNS().then((d) => {
        console.log('GNS GOOD', d);
    }).catch(err => {
        console.log('GNS ERR', err);
    });*/


    
    pos.PUR(1, 'TEST1').then(() => {
        console.log('PUR READY!');
    }).catch(err => {
        console.log('PUR ERR', err);
    });
    
}).catch(err => {
    console.log('ERR', err);
});