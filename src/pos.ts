/**
 * Big thanks to: https://github.com/TransbankDevelopers/transbank-pos-sdk-nodejs
 */


import SerialPort from 'serialport';

//@ts-ignore
import InterByteTimeout from '@serialport/parser-inter-byte-timeout';

const ACK = Buffer.from('06', 'hex');
const STX = Buffer.from('02', 'hex');
const ETX = Buffer.from('03', 'hex');
const EOT = Buffer.from('04', 'hex');
const NAK = Buffer.from('15', 'hex');

const SPR_DOT = Buffer.from('.');
const SPR_1C = Buffer.from('1c', 'hex');

const buffToPrettyString = (bytes: Buffer): string => {
    let result = '';
    bytes.forEach((char: number) => {
        result += (32 <= char && char < 126) ?
            String.fromCharCode(char) :
            `{0x${char.toString(16).padStart(2, '0')}}`
    }, '')
    return result;
}

const buffToRawString = (bytes: Buffer): string => {
    return bytes.toString('hex');
}

const calculateLRC = (data: Buffer): Buffer => {
    let bytes = [];
    let lrc = 0;
    for (let i = 0; i < data.length; i++) {
        bytes.push(data[i]);
    }
    for (let i = 0; i < data.length; i++) {
        lrc ^= bytes[i];
    }

    const b = Buffer.alloc(1);
    b.writeUInt8(lrc);
    return b;
}

const getMessageId = (data: Buffer): string => {
    return data.toString().slice(1, 4);
}

const getMessageType = (data: Buffer): string => {
    return data.toString().slice(4, 6);
}


export interface IPURResponce {
    receipt: string;
    amount: string; // TODO: make amount number
}

export default class POSConnect {
    private connected: boolean = false;
    private connecting: boolean = false;
    private waiting: boolean = false;

    private portName: string = null;


    private port: SerialPort = null;
    private parser: InterByteTimeout = null;


    private debugEnabled: boolean = false;
    private debugCb: (message: string) => void = (msg: string) => console.log(msg);

    private ackCb: (data: Buffer) => void = null;
    private responseCb: (data: Buffer) => void = null;

    // Options
    private responseTimeout: number = 30000;
    private ackTimeout: number = 30000;
    private baudRate: number = 115200;
    private posTimeout: number = 45000;


    constructor(portName: string, options?: {
        responseTimeout?: number,
        baudRate?: number,
        ackTimeout?: number
    }) {
        this.portName = portName;

        if (options) {
            const { baudRate, responseTimeout, ackTimeout } = options;
            if (baudRate) this.baudRate = baudRate;
            if (responseTimeout) this.responseTimeout = responseTimeout;
            if (ackTimeout) this.ackTimeout = ackTimeout;
        }
    }

    public setDebug(debug: boolean): void {
        this.debugEnabled = debug;
    }

    public setDebugCb(cb: (message: string) => void): void {
        this.debugCb = cb;
    }

    private debug(message: string): void {
        if (this.debugEnabled) this.debugCb(message);
    }

    private itsAnACK(data: Buffer): boolean {
        return Buffer.compare(data, ACK) === 0;
    }

    private isAnNAK(data: Buffer): boolean {
        return Buffer.compare(data, NAK) === 0;
    }

    private isAnEOT(data: Buffer): boolean {
        return Buffer.compare(data, EOT) === 0;
    }

    public connect(): Promise<void> {
        this.debug(`Connecting to port ${this.portName} @${this.baudRate}...`);

        return new Promise((resolve, reject) => {
            if (this.connecting === true) {
                reject('Another POS was already connected!');
            }

            if (this.connected) {
                reject('POS already was already connected!');
            }

            this.connecting = true;

            this.port = new SerialPort(this.portName, {
                baudRate: this.baudRate,
                autoOpen: false
            });

            this.port.open((err) => {
                if (err) {
                    reject('Could not open serial connection...');
                }
            });

            this.parser = this.port.pipe(new InterByteTimeout({ interval: 100 }));

            this.parser.on('data', (data: Buffer) => {
                this.debug(`[POS] ==> ${buffToPrettyString(data)} [${buffToRawString(data)}]`);

                if (this.itsAnACK(data)) {
                    this.ackCb(data);
                    return;
                }

                if (this.itsAnACK(Buffer.from(data.toString().slice(0, 1)))) {
                    const len = data.toString().length;
                    data = Buffer.from(data.toString().slice(1, len - 1))
                    this.ackCb(data);
                }

                this.port.write(ACK, () => {
                    this.debug('ACK send!');
                });
                this.responseCb(data);
            });

            this.port.on('open', () => {
                this.connected = true;
                resolve();
            });
        });
    }

    public disconnect(): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.port.isOpen) {
                resolve();
                return;
            }

            this.port.close((error: Error) => {
                if (error) {
                    this.debug(`Error closing port: ${error.message}`);
                    reject(error);
                } else {
                    this.debug('Port closed successfully');
                    resolve();
                }
            });
        });
    }

    private send(payload: Buffer, waitResponse = true, callback: (resp: Buffer) => void = null): Promise<void> {
        return new Promise((resolve, reject) => {
            if (!this.connected) {
                reject(`You have to connect to a POS to send this message: ${buffToRawString(payload)}`);
                return;
            }

            if (this.waiting === true) {
                reject('Another message was already sent and it is still waiting for a response from the POS');
                return;
            }

            this.waiting = true;

            // Assert the ack arrives before the given timeout.
            const timeout = setTimeout(() => {
                this.waiting = false;
                clearTimeout(this.responseTimeout);
                reject(`ACK has not been received in ${this.ackTimeout} ms`);
            }, this.ackTimeout);

            // Defines what should happen when the ACK is received
            this.ackCb = () => {
                clearTimeout(timeout);
                this.debug('ACK received!');
                if (!waitResponse) {
                    this.waiting = false;
                    resolve();
                }
            }

            const lrc = calculateLRC(payload);

            const msg = Buffer.concat([STX, payload, lrc]);

            this.debug(`💻 > ${buffToRawString(msg)} --> ${buffToPrettyString(msg)}`);

            this.port.write(msg, (err) => {
                if (err) {
                    reject('Failed to send message to POS. Maybe it is disconnected.');
                }
            });

            const responseTimeout = setTimeout(() => {
                this.waiting = false;
                reject(`Response of POS has not been received in ${this.posTimeout / 1000} seconds`);
            }, this.posTimeout);


            // Wait for the response and fullfill the Promise
            this.responseCb = (data) => {
                clearTimeout(responseTimeout);
                this.debug(`Response: MsgID: ${getMessageId(data)} MsgType: ${getMessageType(data)}  --> ${buffToPrettyString(data)}`);

                if (callback) callback(data);
                this.waiting = false;
                resolve();
            }
        });
    }

    // Use it for test connection with terminal!
    public GNS(): Promise<string> {
        return new Promise((resolve, reject) => {
            const message = Buffer.concat([
                Buffer.from('GNS'),
                Buffer.from('10'),
                SPR_DOT,
                SPR_1C,
                ETX
            ]);

            const cb = (data: Buffer) => {
                const id = getMessageId(data);
                const type = getMessageType(data);

                if (id === 'GNS' && type === '11') {
                    const parsed = data.toString().slice(7, 16);
                    resolve(parsed);
                    return;
                }
                reject('Wrong message received!');
            };
            this.send(message, true, cb).catch(err => {
                reject(err);
            });
        });
    }

    public PUR(amount: number, receipt: string, ecrNumber: string = '01', merchantId: string = '01'): Promise<IPURResponce> {
        return new Promise((resolve, reject) => {
            const pur12 = Buffer.concat([
                Buffer.from('PUR'),
                Buffer.from('10'),
                SPR_DOT,
                Buffer.from(ecrNumber), // ECR Number
                SPR_1C,
                Buffer.from(receipt), // ECR receipt number
                SPR_1C,
                Buffer.from(amount.toString().padStart(10, '0').slice(0, 10) + '00'), // Transaction amount
                SPR_1C,
                Buffer.from('000000000000'), // Transaction amount #2
                SPR_1C,
                Buffer.from('393830', 'hex'), // Currency code
                SPR_1C,
                Buffer.from('000000'), // Product codes
                SPR_1C,
                // Track 1 Data
                SPR_1C,
                // Track 2 Data
                SPR_1C,
                // Track 3 Data
                SPR_1C,
                Buffer.from('000'), // Spare
                SPR_1C,
                Buffer.from(merchantId), // MerchantId
                SPR_1C,
                Buffer.from('Host Text ...600!'), // Host text
                SPR_1C,
                // PINRequest
                SPR_1C,
                // RNN
                SPR_1C,
                // Entry Mode
                SPR_1C,
                SPR_1C,
                Buffer.from('A'), // MerchantID 5th symbol
                SPR_1C,
                ETX
            ]);

            const pur13 = Buffer.concat([
                Buffer.from('PUR'),
                Buffer.from('13'),
                SPR_DOT,
                ETX
            ]);

            const cb = (data: Buffer) => {
                const id = getMessageId(data);
                const type = getMessageType(data);

                if (id === 'PUR') {
                    switch (type) {
                        case '11': {
                            this.debug('PUR11 received!');
                            break;
                        }
                        case '12': {
                            const respCode = data.toString().slice(7, 9);
                            const ecrReceipt = data.toString().slice(13, 19);
                            const amount = data.toString().slice(19, 31);

                            this.debug(`PUR12 respCode: ${respCode} Receipt: ${ecrReceipt} Amount: ${amount}`);

                            this.send(pur13, false).catch(err => {
                                reject(err);
                            });
                            resolve({ receipt: ecrReceipt, amount });
                            break;
                        }
                        default: {
                            this.debug(`Wrong type: ${type}`);
                        }
                    }
                }
            }

            this.send(pur12, true, cb).catch(err => {
                reject(err);
            });
        });
    }
}